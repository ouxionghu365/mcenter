package endpoint

import "github.com/infraboard/mcube/http/request"

const (
	AppName = "endpoints"
)

type Service interface {
	RPCServer
}

func NewQueryEndpointRequest() *QueryEndpointRequest {
	return &QueryEndpointRequest{
		Page: request.NewDefaultPageRequest(),
	}
}
