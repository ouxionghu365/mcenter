package impl

import (
	"gitee.com/ouxionghu365/mcenter/apps/endpoint"
	"gitee.com/ouxionghu365/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

var (
	svc = &impl{}
)

type impl struct {
	endpoint.UnimplementedRPCServer
	col *mongo.Collection
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(endpoint.AppName)
	return nil
}

func (i *impl) Name() string {
	return endpoint.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	endpoint.RegisterRPCServer(server, i)
}

func init() {
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
