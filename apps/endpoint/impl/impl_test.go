package impl_test

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/endpoint"
	"gitee.com/ouxionghu365/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	impl endpoint.Service
	ctx  = context.Background()
)

func init() {
	tools.DevelopmentSetup()
	impl = app.GetInternalApp(endpoint.AppName).(endpoint.Service)
}
