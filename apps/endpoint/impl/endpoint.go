package impl

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/endpoint"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (i *impl) RegistryEndpoint(ctx context.Context, in *endpoint.RegistryRequest) (
	*endpoint.EndpointSet, error) {
	set := endpoint.NewEndpointSet()
	opt := &options.UpdateOptions{}
	opt.SetUpsert(true)
	for m := range in.Items {
		r := in.Items[m]
		ep := endpoint.NewEndpoint(r)
		_, err := i.col.UpdateOne(ctx,
			bson.M{"_id": ep.Meta.Id},
			bson.M{"$set": ep},
			opt)
		if err != nil {
			return nil, err
		}
		set.Items = append(set.Items, ep)
	}
	return set, nil
}

// endpoint查询接口的实现
func (i *impl) QueryEndpoint(ctx context.Context, in *endpoint.QueryEndpointRequest) (
	*endpoint.EndpointSet, error) {
	set := endpoint.NewEndpointSet()
	//构造查询条件
	filter := bson.M{}
	opts := &options.FindOptions{}
	opts.SetLimit(int64(in.Page.PageSize))
	opts.SetSkip(in.Page.ComputeOffset())
	//执行查询SQL
	cursor, err := i.col.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}

	//获取查询结果
	for cursor.Next(ctx) {
		ins := endpoint.NewDefaultEndpoint()
		if err := cursor.Decode(ins); err != nil {
			return nil, err
		}
		set.Add(ins)
	}
	//统计总数
	set.Total, err = i.col.CountDocuments(ctx, filter)
	if err != nil {
		return nil, err
	}
	return set, nil

}
