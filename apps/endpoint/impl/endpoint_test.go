package impl_test

import (
	"testing"

	"gitee.com/ouxionghu365/mcenter/apps/endpoint"
)

func TestRegistryEndpoint(t *testing.T) {
	req := &endpoint.RegistryRequest{
		Items: []*endpoint.CreateEndpointRequest{},
	}
	req.Items = append(req.Items, &endpoint.CreateEndpointRequest{
		ServiceId: "xxx",
		Method:    "GET",
		Path:      "xxxx",
		Operation: "xxx",
	})
	set, err := impl.RegistryEndpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestQueryEndpoint(t *testing.T) {
	req := endpoint.NewQueryEndpointRequest()
	set, err := impl.QueryEndpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
