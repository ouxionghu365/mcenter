package impl_test

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/policy"
	"gitee.com/ouxionghu365/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	//声明需要测试的对象
	impl policy.Service

	ctx = context.Background()
)


func init() {
	tools.DevelopmentSetup()
	impl= app.GetInternalApp(policy.AppName).(policy.Service)
}