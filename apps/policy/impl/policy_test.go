package impl_test

import (
	"testing"

	"gitee.com/ouxionghu365/mcenter/apps/policy"
)

func TestCreatePolicy(t *testing.T) {
	req := policy.NewCreatePolicyRequest()
	req.Namespace = "default"
	req.RoleId = "chkofoo99ms1cq3e4tpg"
	req.UserId = "ch9i14099ms0v66aih60"
	ins, err := impl.CreatePolicy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestXxx(t *testing.T) {
	req := policy.NewQueryPolicyRequest()
	set, err := impl.QueryPolicy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)

}
