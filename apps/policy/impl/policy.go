package impl

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/policy"
	"gitee.com/ouxionghu365/mcenter/apps/role"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (i *impl) CreatePolicy(ctx context.Context, in *policy.CreatePolicyRequest) (
	*policy.Policy, error) {
	//构建入库实例
	ins := policy.NewPolicy(in)
	_, err := i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}

func (i *impl) QueryPolicy(ctx context.Context, in *policy.QueryPolicyRequest) (
	*policy.PolicySet, error) {
	set := policy.NewPolicySet()

	//构造查询条件
	filter := bson.M{}

	if in.UserId != "" {
		filter["user_id"] = in.UserId
	}

	if in.Namespace != "" {
		filter["namespace"] = in.Namespace
	}

	opts := &options.FindOptions{}
	opts.SetLimit(int64(in.Page.PageSize))
	opts.SetSkip(in.Page.ComputeOffset())

	//查询数据库
	cursor, err := i.col.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}

	for cursor.Next(ctx) {
		ins := policy.NewDefaultPolicy()
		if err := cursor.Decode(ins); err != nil {
			return nil, err
		}
		set.Add(ins)
	}

	if in.WithRole {
		req := role.NewQueryRoleRequest()
		//可变参数
		req.AddRoleId(set.RoleIds()...)
		req.Page.PageSize = set.Len()
		rs, err := i.role.QueryRole(ctx, req)
		if err != nil {
			return nil, err
		}
		//遍历角色列表
		for i := range rs.Items {
			r := rs.Items[i]
			set.SetRole(r)
		}
	}

	//统计总数
	set.Total, err = i.col.CountDocuments(ctx, filter)
	if err != nil {
		return nil, err
	}
	return set, nil
}
