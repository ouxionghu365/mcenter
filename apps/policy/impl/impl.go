package impl

import (
	"gitee.com/ouxionghu365/mcenter/apps/policy"
	"gitee.com/ouxionghu365/mcenter/apps/role"
	"gitee.com/ouxionghu365/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

var (
	svc = &impl{}
)

type impl struct {
	policy.UnimplementedRPCServer
	col  *mongo.Collection
	role role.Service
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(policy.AppName)
	i.role = app.GetInternalApp(role.AppName).(role.Service)
	return nil
}

func (i *impl) Name() string {
	return policy.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	policy.RegisterRPCServer(server, i)
}

func init() {
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}