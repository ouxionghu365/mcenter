package service

import (
	"time"

	"gitee.com/ouxionghu365/mcenter/common/meta"
	"github.com/rs/xid"
)

func NewService(in *CreateServiceRequest) *Service {
	return &Service{
		Meta: meta.NewMeta(),
		Spec: in,
		Credentail: &Credentail{
			ClientId:     xid.New().String(),
			ClientSecret: xid.New().String(),
			CreateAt:     time.Now().Unix(),
		},
	}
}

func NewDefaultService() *Service {
	return NewService(&CreateServiceRequest{})
}
