package service

import (
	"context"
)

const (
	AppName = "service"
)

type ServiceManger interface {
	CreateService(context.Context, *CreateServiceRequest) (*Service, error)
	RPCServer
}
