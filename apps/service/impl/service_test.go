package impl_test

import (
	"testing"

	"gitee.com/ouxionghu365/mcenter/apps/service"
	"gitee.com/ouxionghu365/mcenter/test/tools"
)

func TestCreateService(t *testing.T) {
	req := &service.CreateServiceRequest{
		Domian:    "default",
		Namespace: "default",
		Name:      "mpaas",
	}

	ins, err := impl.CreateService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestDescribeService(t *testing.T) {
	req := &service.DescribeServiceRequest{
		DescribeBy:    service.DESCRIBE_BY_SERVICE_CREDENTAIL_ID,
		DescribeVaule: "chiqa6099ms6pk7pam3g",
	}

	ins, err := impl.DescribeService(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}
