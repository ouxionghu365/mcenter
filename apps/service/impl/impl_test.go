package impl_test

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/service"
	"gitee.com/ouxionghu365/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	impl service.ServiceManger
	ctx  = context.Background()
)

func init() {
	//初始化ioc
	tools.DevelopmentSetup()

	impl = app.GetInternalApp(service.AppName).(service.ServiceManger)
}
