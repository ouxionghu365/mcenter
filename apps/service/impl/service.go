package impl

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/service"
	"go.mongodb.org/mongo-driver/bson"
)

func (i *impl) CreateService(ctx context.Context, in *service.CreateServiceRequest) (
	*service.Service, error) {
	ins := service.NewService(in)
	_, err := i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}

func (i impl) DescribeService(ctx context.Context, in *service.DescribeServiceRequest) (
	*service.Service, error) {
	filter := bson.M{}
	switch in.DescribeBy {
	case service.DESCRIBE_BY_SERVICE_ID:
		filter["_id"] = in.DescribeVaule
	case service.DESCRIBE_BY_SERVICE_CREDENTAIL_ID:
		filter["credentail.client_id"] = in.DescribeVaule
	}

	ins := service.NewDefaultService()
	err := i.col.FindOne(ctx, filter).Decode(ins)
	if err != nil {
		return nil, err
	}
	return ins, nil

}
