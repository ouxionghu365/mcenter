package user_test

import (
	"testing"

	"gitee.com/ouxionghu365/mcenter/apps/user"
)

func TestCreateUserRequest(t *testing.T) {
	req := user.NewCreateUserRequest()
	req.Password = "123456"
	err := req.HashPassword()
	if err != nil {
		t.Log(err)
	}
	t.Log(req.Password)
}
