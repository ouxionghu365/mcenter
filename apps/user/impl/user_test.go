package impl_test

import (
	"testing"

	"gitee.com/ouxionghu365/mcenter/apps/user"
	"gitee.com/ouxionghu365/mcenter/test/tools"
)

func TestNewCreateUserRequest(t *testing.T) {
	req := user.NewCreateUserRequest()
	req.Username = "admin"
	req.Password = "123456"
	ins, err := impl.CreateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestQueryUserRequest(t *testing.T) {
	req := user.NewQueryUserRequest()
	req.Keywords = "adm"
	set, err := impl.QueryUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(tools.MustToJson(set))
}

func TestDescribeUser(t *testing.T) {
	req := user.DescribeUserRequest{
		DescribeBy:    user.DESCRIBE_BY_USERNAME,
		DescribeValue: "admin",
	}
	ins, err := impl.DescribeUser(ctx, &req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(tools.MustToJson(ins))
	err = ins.CheckPassword("123456")
	if err != nil {
		t.Fatal(err)
	}
}
