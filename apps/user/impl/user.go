package impl

import (
	"context"
	"fmt"

	"gitee.com/ouxionghu365/mcenter/apps/user"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (i *impl) CreateUser(ctx context.Context, in *user.CreateUserRequest) (
	*user.User, error) {
	ins, err := user.New(in)
	if err != nil {
		return nil, err
	}

	_, err = i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}

func (i *impl) QueryUser(ctx context.Context, in *user.QueryUserRequest) (*user.UserSet, error) {
	set := user.NewUserSet()

	//构造查询条件
	filter := bson.M{}
	if in.Keywords != "" {
		filter["username"] = bson.M{"$regex": in.Keywords, "$options": "im"}
	}
	opts := &options.FindOptions{}
	opts.SetLimit(int64(in.Page.PageSize))
	opts.SetSkip(in.Page.ComputeOffset())

	collections, err := i.col.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}

	//执行查询SQL
	for collections.Next(ctx) {
		ins := user.NewDefaultUser()
		if err := collections.Decode(ins); err != nil {
			return nil, err
		}
		ins.Desense()
		set.Add(ins)
	}

	//统计总数
	set.Total, err = i.col.CountDocuments(ctx, filter)
	if err != nil {
		return nil, err
	}
	return set, nil
}

func (i *impl) DescribeUser(ctx context.Context, in *user.DescribeUserRequest) (
	*user.User, error) {
	filter := bson.M{}
	switch in.DescribeBy {
	case user.DESCRIBE_BY_USER_ID:
		filter["_id"] = in.DescribeValue
	case user.DESCRIBE_BY_USERNAME:
		filter["username"] = in.DescribeValue
	}
	ins := user.NewDefaultUser()
	err := i.col.FindOne(ctx, filter).Decode(ins)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("%s", "用户名或者密码不对!!!")
		}
		return nil, err
	}
	return ins, nil
}
