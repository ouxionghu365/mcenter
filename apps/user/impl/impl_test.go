package impl_test

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/user"
	"gitee.com/ouxionghu365/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
)

var (
	impl user.Service
	ctx  = context.Background()
)

func init() {
	tools.DevelopmentSetup()
	impl = app.GetInternalApp(user.AppName).(user.Service)
}
