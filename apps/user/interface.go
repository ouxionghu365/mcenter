package user

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/domain"
	"gitee.com/ouxionghu365/mcenter/common/validator"
	"github.com/infraboard/mcube/http/request"
)

const (
	AppName = "users"
)

// 内部接口 模块内调用
type Service interface {
	CreateUser(context.Context, *CreateUserRequest) (*User, error)
	// UpdateUser(context.Context, *UpdataUserRequest) (*User, error)
	// DeleteUser(context.Context, *DeleteUserRequest) (*User, error)
	RPCServer
}

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{}
}

func (req *CreateUserRequest) Validate() error {
	if req.Domain == "" {
		req.Domain = domain.DEFAULT_DOMAIN
	}
	return validator.Validate(req)
}

func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		Page: request.NewDefaultPageRequest(),
	}
}

func NewDescribeUserRequestByUsername(username string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy:    DESCRIBE_BY_USERNAME,
		DescribeValue: username,
	}
}
