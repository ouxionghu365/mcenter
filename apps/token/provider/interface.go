package provider

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/token"
)

type Issuer interface {
	Config() error
	TokenIssuer
}

type TokenIssuer interface {
	IssueToken(context.Context, *token.IssueTokenRequest) (*token.Token, error)
}

//定义IOC规范 满足上面要求规范才能成为Issuer
