package password

import (
	"context"
	"fmt"

	"gitee.com/ouxionghu365/mcenter/apps/namespace"
	"gitee.com/ouxionghu365/mcenter/apps/token"
	"gitee.com/ouxionghu365/mcenter/apps/token/provider"
	"gitee.com/ouxionghu365/mcenter/apps/user"
	"gitee.com/ouxionghu365/mcenter/common/logger"
	"github.com/infraboard/mcube/app"
	"github.com/rs/xid"
)

type issuer struct {
	user user.Service
}

func (i *issuer) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (
	*token.Token, error) {
	//查询该用户
	req := user.NewDescribeUserRequestByUsername(in.Username)
	u, err := i.user.DescribeUser(ctx, req)
	if err != nil {
		return nil, err
	}

	//检查密码
	err = u.CheckPassword(in.Password)
	if err != nil {
		logger.L().Debug().Msg(err.Error())
		return nil, fmt.Errorf("用户名或者密码错误")
	}

	//颁发令牌
	tk := token.NewToken()
	tk.AccessToken = xid.New().String()
	tk.RefreshToken = xid.New().String()
	tk.UserId = u.Meta.Id
	tk.Username = u.Spec.Username
	tk.Namespace = namespace.DEFAULT_NAMESPACE

	return tk, nil
}

func (i *issuer) Config() error {
	i.user = app.GetInternalApp(user.AppName).(user.Service)
	return nil
}

func init() {
	provider.Registry(token.GRANT_TYPE_PASSWORD, &issuer{})
}
