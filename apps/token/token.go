package token

import (
	"errors"
	"time"
)

func NewToken() *Token {
	return &Token{
		IssueAt:          time.Now().Unix(),
		AccessExpiredAt:  600,
		RefreshExpiredAt: 600 * 4,
		Status:           &Status{},
		Meta:             map[string]string{},
	}
}

// 检查Token可用性
func (t *Token) CheckAliable() error {
	if t.Status.IsBlock {
		return errors.New(t.Status.BlockReason)
	}

	//检查是否过期
	duration := time.Since(t.ExpiredTime())
	if duration >= 0 {
		return errors.New("令牌已经过期")
	}
	return nil
}

func (t *Token) ExpiredTime() time.Time {
	dura := time.Duration(t.AccessExpiredAt * int64(time.Second))
	return time.Unix(t.IssueAt, 0).Add(dura)
}
