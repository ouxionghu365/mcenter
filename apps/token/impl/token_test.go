package impl_test

import (
	"testing"

	"gitee.com/ouxionghu365/mcenter/apps/token"
	"github.com/infraboard/mcube/exception"
)

func TestIssueToken(t *testing.T) {
	req := &token.IssueTokenRequest{
		Username: "admin",
		Password: "123456",
	}

	tk, err := impl.IssueToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

// chc9gpo99ms5182ddnr0
func TestValidateToken(t *testing.T) {
	req := token.NewValidateTokenRequest("chc9nk099ms3du6fsrgg")
	tk, err := impl.ValidateToken(ctx, req)
	if err != nil {
		// 需要加逻辑 才能是exception.APIException
		t.Log(err.(exception.APIException).ToJson())
		t.Fatal(err)
	}
	t.Log(tk)
}
