package impl

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/token"
	"gitee.com/ouxionghu365/mcenter/apps/token/provider"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (i *impl) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (
	*token.Token, error) {
	if err := in.Validate(); err != nil {
		return nil, err
	}

	//颁发令牌
	issuer := provider.Get(in.GrantType)
	ins, err := issuer.IssueToken(ctx, in)
	if err != nil {
		return nil, err
	}

	//入库
	_, err = i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}
	return ins, nil
}

func (i *impl) ValidateToken(ctx context.Context, in *token.ValidateTokenRequest) (
	*token.Token, error) {
	tk := token.NewToken()
	err := i.col.FindOne(ctx, bson.M{"_id": in.AccessToken}).Decode(tk)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("%s not found", in.AccessToken)
		}
		return nil, err
	}
	err = tk.CheckAliable()
	if err != nil {
		return nil, err
	}
	return tk, nil
}
