package impl

import (
	"gitee.com/ouxionghu365/mcenter/apps/token"
	"gitee.com/ouxionghu365/mcenter/apps/token/provider"
	_ "gitee.com/ouxionghu365/mcenter/apps/token/provider/all"
	"gitee.com/ouxionghu365/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

var (
	svc = &impl{}
)

type impl struct {
	token.UnimplementedRPCServer
	col *mongo.Collection
}

// 实例化初始类
func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection("tokens")

	return provider.Init()
}

func (i *impl) Name() string {
	return token.Appname
}

func (i *impl) Registry(server *grpc.Server) {
	token.RegisterRPCServer(server, i)
}

func init() {
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
