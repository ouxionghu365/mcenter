package token

import (
	"context"
	"errors"
)

const (
	Appname = "tokens"
)

type Service interface {
	IssueToken(context.Context, *IssueTokenRequest) (*Token, error)
	RPCServer
}

func NewValidateTokenRequest(tk string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: tk,
	}
}

func NewIssueTokenRequest() *IssueTokenRequest {
	return &IssueTokenRequest{}
}

func (req *IssueTokenRequest) Validate() error {
	switch req.GrantType {
	case GRANT_TYPE_PASSWORD, GRANT_TYPE_LDAP:
		if req.Username == "" || req.Password == "" {
			return errors.New("用户名或者密码缺失")
		}
	}
	return nil
}
