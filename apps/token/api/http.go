package api

import (
	"gitee.com/ouxionghu365/mcenter/apps/token"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
)

var (
	h = &handler{}
)

type handler struct {
	service token.Service
}

func (h *handler) Config() error {
	h.service = app.GetInternalApp(token.Appname).(token.Service)
	return nil
}

func (h *handler) Name() string {
	return token.Appname
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"登录"}
	ws.Route(ws.POST("/").To(h.IssueToken).
		Doc("颁发令牌").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		//设置该接口需不需要认证
		Metadata("auth", false).
		Reads(token.IssueTokenRequest{}).Writes(token.Token{}).
		Returns(200, "Ok", token.Token{}))
}

func init() {
	app.RegistryRESTfulApp(h)
}
