package api

import (
	"fmt"

	"gitee.com/ouxionghu365/mcenter/apps/token"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/http/response"
)

type Exception struct {
	Code    int
	Message string
}

func (h *handler) IssueToken(r *restful.Request, w *restful.Response) {
	in := token.NewIssueTokenRequest()
	err := r.ReadEntity(in)
	fmt.Println(in.Password)
	fmt.Println(in.Username)
	if err != nil {
		response.Failed(w, err)
		return
	}
	tk, err := h.service.IssueToken(r.Request.Context(), in)
	if err != nil {
		response.Failed(w, err)
		return
	}

	w.WriteEntity(tk)
}
