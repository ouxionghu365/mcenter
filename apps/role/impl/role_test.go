package impl_test

import (
	"testing"

	"gitee.com/ouxionghu365/mcenter/apps/role"
)

func TestCreateRole(t *testing.T) {
	req := role.NewCreateRoleRequest()
	req.Name = "admin"
	req.AddFeature(&role.Feature{
		ServiceId:  "chiqa6099ms6pk7pam30",
		HttpMethod: "GET",
		HttpPath:   "xxxx",
	})
	ins, err := impl.CreateRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryRole(t *testing.T) {
	req := role.NewQueryRoleRequest()
	req.AddRoleId("chkofoo99ms1cq3e4tpg")
	set, err := impl.QueryRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
