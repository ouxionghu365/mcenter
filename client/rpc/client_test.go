package rpc_test

import (
	"context"
	"testing"

	"gitee.com/ouxionghu365/mcenter/apps/token"
	"gitee.com/ouxionghu365/mcenter/client/rpc"
)

func TestValidateTokenRequest(t *testing.T) {
	req := token.NewValidateTokenRequest("chgodto99ms0o17ssslg")
	tk, err := client.Token().ValidateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

var (
	client *rpc.ClientSet
	ctx    = context.Background()
)

func init() {
	c, err := rpc.NewClient(rpc.NewDefaultConfig())
	if err != nil {
		panic(err)
	}
	client = c
}
