package rpc

import (
	"context"

	"gitee.com/ouxionghu365/mcenter/apps/endpoint"
	"gitee.com/ouxionghu365/mcenter/apps/token"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func NewClient(conf *Config) (*ClientSet, error) {
	ctx, cancel := context.WithTimeout(context.Background(), conf.Timeout())
	defer cancel()
	conn, err := grpc.DialContext(
		ctx,
		conf.Address,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conf: conf,
		conn: conn,
	}, nil
}

type ClientSet struct {
	conf *Config
	conn *grpc.ClientConn
}

// token rpc客户端
func (c *ClientSet) Token() token.RPCClient {
	return token.NewRPCClient(c.conn)
}

// endpoint的rpc客户端
func (c *ClientSet) Endpoint() endpoint.RPCClient {
	return endpoint.NewRPCClient(c.conn)
}
