package auth

import (
	"strings"

	"gitee.com/ouxionghu365/mcenter/apps/token"
	"gitee.com/ouxionghu365/mcenter/client/rpc"
	"gitee.com/ouxionghu365/mcenter/common/logger"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/response"
)

type httpAuther struct {
	client *rpc.ClientSet
}

func NewHttpAuther(client *rpc.ClientSet) *httpAuther {
	return &httpAuther{
		client: client,
	}
}

func (a *httpAuther) AuthFunc(
	req *restful.Request,
	resp *restful.Response,
	next *restful.FilterChain) {
	logger.L().Info().Msgf("%s", req)
	//从Header中获取token
	authHeader := req.HeaderParameter(token.TOKEN_HEADER_KEY)
	tk1 := strings.Split(authHeader, " ")
	if len(tk1) != 2 {
		response.Failed(resp, exception.NewUnauthorized("令牌不合法,格式:Authorization: breaer xxxx"))
		return
	}
	tk := tk1[1]
	logger.L().Debug().Msgf("get token: %s", tk)

	//检查Token的合法性，需要通过RPC进行检查
	tkObj, err := a.client.Token().ValidateToken(
		req.Request.Context(), token.NewValidateTokenRequest(tk),
	)

	if err != nil {
		response.Failed(resp, exception.NewUnauthorized("令牌校验不合法，%s", err))
		return
	}

	//放入上下文
	req.SetAttribute(token.ATTRIBUTE_TOKEN_KEY, tkObj)

	//交给下面流程处理
	next.ProcessFilter(req, resp)
}
