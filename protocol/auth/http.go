package auth

import (
	"fmt"
	"strings"

	"gitee.com/ouxionghu365/mcenter/apps/endpoint"
	"gitee.com/ouxionghu365/mcenter/apps/token"
	"gitee.com/ouxionghu365/mcenter/common/logger"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/response"
)

func NewHttpAuther() *httpAuther {
	return &httpAuther{
		token: app.GetInternalApp(token.Appname).(token.Service),
	}
}

type httpAuther struct {
	token token.Service
}

func (a *httpAuther) AuthFunc(
	req *restful.Request,
	resp *restful.Response,
	next *restful.FilterChain) {
	accessRoute := req.SelectedRoute()
	ep := &endpoint.Endpoint{
		Spec: &endpoint.CreateEndpointRequest{
			ServiceId: "",
			Method:    accessRoute.Method(),
			Path:      accessRoute.Path(),
			Operation: accessRoute.Operation(),
		},
	}
	isAuth := accessRoute.Metadata()["auth"]
	if isAuth != nil {
		ep.Spec.Auth = isAuth.(bool)
	}
	fmt.Printf("%v", ep)

	if ep.Spec.Auth {
		// 从Header中获取token
		authHeader := req.HeaderParameter(token.TOKEN_HEADER_KEY)
		tkl := strings.Split(authHeader, " ")
		if len(tkl) != 2 {
			response.Failed(resp, exception.NewUnauthorized("令牌不合法, 格式： Authorization: breaer xxxx"))
			return
		}

		tk := tkl[1]
		logger.L().Debug().Msgf("get token: %s", tk)

		// 检查Token的合法性, 需要通过RPC进行检查
		tkObj, err := a.token.ValidateToken(
			req.Request.Context(),
			token.NewValidateTokenRequest(tk),
		)
		if err != nil {
			response.Failed(resp, exception.NewUnauthorized("令牌校验不合法, %s", err))
			return
		}

		// 放入上下文
		req.SetAttribute(token.ATTRIBUTE_TOKEN_KEY, tkObj)
	}

	// 交给下个处理
	next.ProcessFilter(req, resp)
}
