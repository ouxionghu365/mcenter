package protocol

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitee.com/ouxionghu365/mcenter/apps/endpoint"
	"gitee.com/ouxionghu365/mcenter/common/logger"
	"gitee.com/ouxionghu365/mcenter/conf"
	"gitee.com/ouxionghu365/mcenter/protocol/auth"
	"gitee.com/ouxionghu365/mcenter/swagger"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
)

func NewHTTPService() *HTTPService {
	r := restful.DefaultContainer
	cors := restful.CrossOriginResourceSharing{
		AllowedHeaders: []string{"*"},
		AllowedDomains: []string{"*"},
		AllowedMethods: []string{"HEAD", "OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE"},
		CookiesAllowed: false,
		Container:      r,
	}
	r.Filter(cors.Filter)
	r.Filter(auth.NewHttpAuther().AuthFunc)

	server := &http.Server{
		ReadHeaderTimeout: 60 * time.Second,
		ReadTimeout:       60 * time.Second,
		WriteTimeout:      60 * time.Second,
		IdleTimeout:       60 * time.Second,
		MaxHeaderBytes:    1 << 20, //1M
		Addr:              conf.C().App.HTTP.Addr(),
		Handler:           r,
	}

	return &HTTPService{
		r:      r,
		server: server,
		c:      conf.C(),
	}

}

type HTTPService struct {
	r      *restful.Container
	c      *conf.Config
	server *http.Server
}

func (s *HTTPService) PathPrefix() string {
	return fmt.Sprintf("/%s/api", s.c.App.Name)
}

// start 启动服务
func (s *HTTPService) Start() error {
	//装配子服务路由
	app.LoadRESTfulApp(s.PathPrefix(), s.r)

	//获取当前已经装载的所有web Service
	es := endpoint.NewRegistryRequest()
	wss := s.r.RegisteredWebServices()
	for i := range wss {
		ws := wss[i]
		// 该web service 所有路由
		routes := ws.Routes()
		for m := range routes {
			route := routes[m]
			ep := &endpoint.CreateEndpointRequest{
				ServiceId: "",
				Method:    route.Method,
				Path:      route.Path,
				Operation: route.Operation,
			}
			es.Add(ep)
		}

	}
	fmt.Printf("%v\n", es)

	//api doc
	config := restfulspec.Config{
		WebServices:                   restful.RegisteredWebServices(), // you control what services are visible
		APIPath:                       "/apidocs.json",
		PostBuildSwaggerObjectHandler: swagger.Docs,
		DefinitionNameHandler: func(name string) string {
			if name == "state" || name == "sizeCache" || name == "unknownFields" {
				return ""
			}
			return name
		},
	}
	s.r.Add(restfulspec.NewOpenAPIService(config))
	logger.L().Info().Msgf("Get the API using http://%s%s", s.c.App.HTTP.Addr(), config.APIPath)

	logger.L().Info().Msgf("HTTP服务启动成功, 监听地址: %s", s.server.Addr)
	if err := s.server.ListenAndServe(); err != nil {
		if err == http.ErrServerClosed {
			logger.L().Info().Msgf("service is stopped")
		}
		return fmt.Errorf("start service error, %s", err.Error())
	}
	return nil
}

// stop 停止server
func (s *HTTPService) Stop() error {
	logger.L().Info().Msgf("start graceful shutdown")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := s.server.Shutdown(ctx); err != nil {
		logger.L().Error().Msgf("graceful shutdown timeout,force exit")
	}
	return nil
}
