package protocol

import (
	"net"

	"gitee.com/ouxionghu365/mcenter/common/logger"
	"gitee.com/ouxionghu365/mcenter/conf"
	"gitee.com/ouxionghu365/mcenter/protocol/auth"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/grpc/middleware/recovery"
	"google.golang.org/grpc"
)

func NewGRPCService() *GRPCService {
	rc := recovery.NewInterceptor(recovery.NewZapRecoveryHandler())
	grpcServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		rc.UnaryServerInterceptor(),
		auth.NewGrpcAuther().AuthFunc,
	))

	return &GRPCService{
		svr: grpcServer,
		c:   conf.C(),
	}
}

type GRPCService struct {
	svr *grpc.Server
	c   *conf.Config
}

// start 启动GRPC服务
func (s *GRPCService) Start() {
	app.LoadGrpcApp(s.svr)

	//启动一个tcp监听
	lis, err := net.Listen("tcp", s.c.App.GRPC.Addr())
	if err != nil {
		logger.L().Debug().Msgf("listen grpc tcp conn error,%s", err)
		return
	}
	logger.L().Info().Msgf("GRPC 服务监听地址: %s", s.c.App.GRPC.Addr())
	if err := s.svr.Serve(lis); err != nil {
		if err == grpc.ErrServerStopped {
			logger.L().Info().Msg("server is stopped")
		}

		logger.L().Error().Msgf("start grpc service error, %s", err.Error())
		return
	}
}

// Stop 启动GRPC服务
func (s *GRPCService) Stop() error {
	s.svr.GracefulStop()
	return nil
}
