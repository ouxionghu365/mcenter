package tools

import (
	_ "gitee.com/ouxionghu365/mcenter/apps"
	"gitee.com/ouxionghu365/mcenter/conf"
	"github.com/infraboard/mcube/app"
)

func DevelopmentSetup() {
	err := conf.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}

	if err := app.InitAllApp(); err != nil {
		panic(err)
	}
}
