package format_test

import (
	"fmt"
	"testing"

	"gitee.com/ouxionghu365/mcenter/common/format"
)

func TestPrettify(t *testing.T) {
	str1 := "111222333"
	str2 := format.Prettify(str1)
	fmt.Println(str2)
}
